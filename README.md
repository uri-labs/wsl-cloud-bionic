# WSL Cloud / Bionic

A [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/faq) distribution that includes:

* Ubuntu 18.04.01+
* Cloud Platform CLIs for Google Cloud Platform, AWS, and Azure
* Docker client


## Installation


Download from https://bitbucket.com/uri-labs

Click 'wsl-cloud-bionic' 
Click 'Downloads'
Click 'Download repository'

Extract it to a directory on your system drive.

Double Click wslc-bionic.exe to launch the shell. It will take a few minutes to install. Once it does, the shell will close. Reopen it and you should be logged in as user uritest.

## Usage

**Set distribution as default**

>***Will allow VS Code and CMD to open WSL Cloud***


    wslconfig /s wslc-bionic



**List registered distributions**

    wslconfig /l

**Uninstall distribution**

>***This will completely remove the extracted files!***


    wslconfig /u  wslc-bionic






## WSL Prerequisites

* Windows 10

* WSL Enabled

**To enable WSL:**

Open PowerShell as Administrator and run:


    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux


Restart your computer when prompted.

## License

[MIT]()

## Contributors 

Andrew Druffner, URI

## Dependencies / References

[WSLDL](https://github.com/yuk7/wsldl)
[WSL Distro Launcher Reference Implementation](https://github.com/Microsoft/WSL-DistroLauncher)